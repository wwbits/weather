﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WeatherApp.Infrastructure;
using WeatherApp.Models;

namespace WeatherApp.Controllers
{
    public class WeatherController : Controller
    {
        private readonly IWeatherProvider provider;

        public WeatherController(IWeatherProvider provider)
        {
            this.provider = provider;
        }


        [HttpPost]
        public async Task<IActionResult> Index(string cityName)
        {
            WeatherModel model = new WeatherModel();
            model.CityName = cityName;

            var weather = await provider.GetWeatherAsync(cityName);
            model.Temperature = weather.TemperatureCelsius;
            model.Pressure = weather.Pressure;
            model.Humidity = weather.Humidity;

            return View(model);
        }
    }
}
