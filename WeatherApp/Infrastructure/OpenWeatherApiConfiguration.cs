﻿namespace WeatherApp.Infrastructure
{
    public class OpenWeatherApiConfiguration
    {
        public string AppId { get; set; }
        public string UrlTemplate { get; set; }
    }
}
