﻿using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using WeatherApp.BusinessEntities;

namespace WeatherApp.Infrastructure
{

    public class OpenWeatherApiProvider : IWeatherProvider
    {
        private readonly OpenWeatherApiConfiguration config;

        public OpenWeatherApiProvider(OpenWeatherApiConfiguration config)
        {
            this.config = config;
        }

        public async Task<Weather> GetWeatherAsync(string cityName)
        {
            string url = string.Format(config.UrlTemplate, cityName, config.AppId);

            WebClient wc = new WebClient();

            var response = await wc.DownloadStringTaskAsync(url);

            return ConverJsonToObject(response);
        }

        private Weather ConverJsonToObject(string json)
        {
            JObject responseJson = JObject.Parse(json);//todo: what about not well formed response
            JToken main = responseJson["main"];
            var temperature = Convert.ToDecimal((string)main["temp"], CultureInfo.InvariantCulture);
            var pressure = Convert.ToDecimal((string)main["pressure"], CultureInfo.InvariantCulture);
            var humidity = Convert.ToDecimal((string)main["humidity"], CultureInfo.InvariantCulture);
            return new Weather(temperature, pressure, humidity);
        }
    }
}
