﻿using System.Threading.Tasks;
using WeatherApp.BusinessEntities;

namespace WeatherApp.Infrastructure
{
    public interface IWeatherProvider
    {
        Task<Weather> GetWeatherAsync(string cityName);
    }
}