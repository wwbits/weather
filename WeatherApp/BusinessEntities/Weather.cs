﻿namespace WeatherApp.BusinessEntities
{
    public class Weather
    {
        public Weather(decimal temperatureCelsius,
            decimal pressure,
            decimal humidity)
        {
            TemperatureCelsius = temperatureCelsius;
            Pressure = pressure;
            Humidity = humidity;
        }

        public decimal TemperatureCelsius { get; }

        public decimal Pressure { get; }

        public decimal Humidity { get; }
    }
}
